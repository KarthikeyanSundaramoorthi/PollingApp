import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './auth/auth-guard.service';
import { UAuthGuard } from './auth/Uauth-guard.service';
import { HomeComponent } from './home/home.component';
import { AdminloginComponent } from './admin/adminlogin/adminlogin.component';
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { UserloginComponent } from './user/userlogin/userlogin.component';
import { UserpollComponent } from './user/userpoll/userpoll.component';

const routes: Routes = [
  { path:"", redirectTo:"home",pathMatch:"full"},//to handle default page redirection
  { path:"home", component: HomeComponent},
  { path:"admin", component: AdminloginComponent},
  {
     path:"admin/dashboard", component: AdmindashboardComponent,canActivate: [AuthGuard]
  },
  { path:"user", component: UserloginComponent},
  {
    path:"user/polls", component: UserpollComponent,canActivate: [UAuthGuard]
  }
]


@NgModule({
	imports:[RouterModule.forRoot(routes)],
	exports:[RouterModule]
})

export class AppRoutingModule{

}

export class Questions{
  
  public id: number;
  public question: string;
  

  constructor(id: number, question:string) { 
  	this.id = id;
  	this.question = question
  }
}

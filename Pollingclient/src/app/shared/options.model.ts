
export class Options{

  public id: number;
  public q_id: number;
  public options: string;
  public option_votes: number;

  constructor(id: number, q_id:number, options: string, option_votes: number) { 
    this.id = id;
  	this.q_id = q_id;
  	this.options = options;
  	this.option_votes = option_votes;
  }
}

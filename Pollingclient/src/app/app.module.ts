import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth-guard.service';
import { UAuthGuard } from './auth/Uauth-guard.service';
import { AuthService } from './auth/auth.service';
import { AppRoutingModule } from './app.routing';
import { AdminloginComponent } from './admin/adminlogin/adminlogin.component';
import { AdmindashboardComponent } from './admin/admindashboard/admindashboard.component';
import { UserloginComponent } from './user/userlogin/userlogin.component';
import { UserpollComponent } from './user/userpoll/userpoll.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminloginComponent,
    AdmindashboardComponent,
    UserloginComponent,
    UserpollComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [AuthGuard, AuthService, UAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

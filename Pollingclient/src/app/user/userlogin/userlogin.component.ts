import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {

  constructor(private authservice: AuthService, private router: Router) { }

  ngOnInit() {
  }

  signIn(form: NgForm){
  	const email = form.value.email;
  	const pass = form.value.password;
  	this.authservice.signInUser(email, pass, 'user').subscribe((res)=>{
      const data = (res.json()).data;
      if(data.length > 0){
        this.authservice.tokenUser = data[0]['id'];
        this.router.navigate(['/user/polls']);
      }
      else{
        form.reset();
        alert("Invalid user nama or password");
      }
    });;
  }
}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import 'rxjs/Rx';
import { AuthService } from '../../auth/auth.service';
import { Questions } from '../../shared/questions.model';
import { Options } from '../../shared/options.model';

@Component({
  selector: 'app-userpoll',
  templateUrl: './userpoll.component.html',
  styleUrls: ['./userpoll.component.css']
})
export class UserpollComponent implements OnInit {

  constructor(private authservice: AuthService) { }
  questions:Questions[] = [];
  options:Options[] = [];
  ngOnInit() {
  	this.authservice.getData().subscribe((res)=>{
  		const data = (res.json());
  		this.questions = data.questions;
  		this.options = data.options;
		});
  }

  userPolls(form:NgForm){
  	let optionvalue = [];
  	for(let i = 0;i<this.questions.length;i++){

  		optionvalue.push(form.value['options_radio_'+this.questions[i]['id']]);
  	}

  	this.authservice.addPolls(optionvalue).subscribe((res)=>{
  		let data = res.json();
  		if(data.status){
  			form.reset();
  		}
  		else{
  			alert("Error occured in server");
  		}
  	});
  }

}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {

  constructor(private authservice: AuthService, private router: Router) { }

  ngOnInit() {
  }

  signIn(form: NgForm){
  	const email = form.value.email;
  	const pass = form.value.password;
  	this.authservice.signInUser(email, pass, 'admin').subscribe((res)=>{
      const data = (res.json()).data;
      if(data.length > 0){
        this.authservice.tokenAdmin = data[0]['id'];
        this.router.navigate(['/admin/dashboard']);
      }
      else{
        form.reset();
        alert("Invalid user nama or password");
      }
    });;
  }
}

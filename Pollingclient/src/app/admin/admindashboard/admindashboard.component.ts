import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import 'rxjs/Rx';
import { AuthService } from '../../auth/auth.service';
import { Questions } from '../../shared/questions.model';
import { Options } from '../../shared/options.model';

@Component({
  selector: 'app-admindashboard',
  templateUrl: './admindashboard.component.html',
  styleUrls: ['./admindashboard.component.css']
})
export class AdmindashboardComponent implements OnInit {

  constructor(private authservice: AuthService) { }
  questions:Questions[] = [];
  options:Options[] = [];
  ngOnInit() {
  	this.authservice.getData().subscribe((res)=>{
  		const data = (res.json());
  		this.questions = data.questions;
  		this.options = data.options;
		});
  }

  addQues(form:NgForm){
  	let obj = {
  		"q":form.value.ques,
  		"options":[]
  	};
  	obj.options.push(form.value.opt1,form.value.opt2);
  	if(form.value.opt3 != "")obj.options.push(form.value.opt3);
  	if(form.value.opt4 != "")obj.options.push(form.value.opt4);
  	if(form.value.opt5 != "")obj.options.push(form.value.opt5);
  	this.authservice.addQuestion(obj).subscribe((res)=>{
  		let data = res.json();
  		if(data.status){
  			form.reset();
  			this.authservice.getData().subscribe((res)=>{
		  		const data = (res.json());
		  		this.questions = data.questions;
		  		this.options = data.options;
				});
  		}
  		else{
  			alert("Error occured while adding");
  		}
  	});
  }
}

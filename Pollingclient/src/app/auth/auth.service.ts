import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class AuthService {
  tokenUser: string;
  tokenAdmin: string;

  constructor(private router: Router, private http: Http) {}

  server: string = 'http://localhost:3000/';
  signInUser(email: string, password: string, type: string) {
    let data = {n:email,p:password,t:type};
    return this.http.post(this.server+'services/verify',data);
  }

  getData() {
    //this.isAuthenticated();
    return this.http.get(this.server+'services/fetchdata');
  }

  addQuestion(obj) {
    //this.isAuthenticated();
    return this.http.post(this.server+'services/addquestion',obj);
  }

  addPolls(arr) {
    //this.isAuthenticated();
    return this.http.post(this.server+'services/addPolls',{ar:arr});
  }

  logout() {
    this.tokenUser = null;
    this.tokenAdmin = null;
  }

  
  isAuthenticatedUser() {
    if(this.tokenUser == null)
    this.router.navigate(['/home']);
    return this.tokenUser != null;
  }

  isAuthenticatedAdmin() {
    if(this.tokenAdmin == null)
    this.router.navigate(['/home']);
    return this.tokenAdmin != null;
  }
}

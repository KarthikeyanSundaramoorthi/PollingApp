import { PollingclientPage } from './app.po';

describe('pollingclient App', () => {
  let page: PollingclientPage;

  beforeEach(() => {
    page = new PollingclientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

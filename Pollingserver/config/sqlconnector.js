var mysql = require('mysql');

var con_obj = {
    connectionLimit : 100, 
    host     : 'localhost',
    user     : 'root',
    password : "",
    database : "polling",
    debug    : false
};

var db = mysql.createConnection(con_obj);
var pool = mysql.createPool(con_obj);

module.exports.db = db;
module.exports.pool = pool;
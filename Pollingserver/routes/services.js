var express = require('express');
const router = express.Router();
const database = require('../config/sqlconnector').pool;

/* verify login. */
router.post('/verify', function(req, res, next) {
  database.getConnection( (err, conn) => {
    if(err) {
      return res.json({ status:0,msg:'Unable to connect to db!', error:err });
    }
    conn.query("SELECT id from master_user WHERE type='"+req.body.t+"' and password = '"+req.body.p+"' and name = '"+req.body.n+"'", (err, rows) => {
      if(err) {
        return res.json({ status:0, msg:'Unable to verify!', error:err });
      }
      res.status(200).json({ status:1, data:rows });
      conn.release();
    });
    conn.on('error', (err) => {
      return res.json({ status:0, msg:'DB Connection problem!', error:err });
      conn.release();
    });

  });
});

/* fetch questions and options with votes login. */
router.get('/fetchdata', function(req, res, next) {
  database.getConnection( (err, conn) => {
    if(err) {
      return res.json({ status:0,msg:'Unable to connect to db!', error:err });
    }
    conn.query("select id, question from m_questions", (err, rows) => {
      if(err) {
        return res.json({ status:0, msg:'Unable to get data!', error:err });
      }
      conn.query("select id, q_id, options, option_votes from m_options", (err, optrows) => {
        if(err) {
          return res.json({ status:0, msg:'Unable to get data!', error:err });
        }
        res.status(200).json({ status:1, questions:rows, options:optrows });
        conn.release();
      });
    });
    conn.on('error', (err) => {
      return res.json({ status:0, msg:'DB Connection problem!', error:err });
      conn.release();
    });

  });
});

/*add question*/
router.post('/addquestion', function(req, res, next) {
  database.getConnection( (err, conn) => {
    if(err) {
      return res.json({ status:0,msg:'Unable to connect to db!', error:err });
    }
    conn.query("insert into m_questions (question) values ('"+req.body.q+"')", (err, rows) => {
      if(err) {
        return res.json({ status:0, msg:'Unable to insert question!', error:err });
      }
      if(rows.affectedRows) {
        var optionsArr = req.body.options;
        var optionsQuery = "insert into m_options (options,q_id,option_votes) values ";
        for (var i = 0; i < optionsArr.length; i++) {
          optionsQuery += "('"+optionsArr[i]+"',"+rows.insertId+",0),";
        }
        optionsQuery = optionsQuery.slice(0, -1);
        conn.query(optionsQuery, (err, rowss) => {
          if(err) {
            return res.json({ status:0, msg:'Unable to insert options!', error:err });
          }
          if(rowss.affectedRows) {
            res.status(200).json({ status:1, data:rows, datas:rowss });
            conn.release();
          }
        });
      }
    });
    conn.on('error', (err) => {
      return res.json({ status:0, msg:'DB Connection problem!', error:err });
    });
  });
});

/*add polls*/
router.post('/addPolls', function(req, res, next) {
  database.getConnection( (err, conn) => {
    if(err) {
      return res.json({ status:0,msg:'Unable to connect to db!', error:err });
    }
    var values = (req.body.ar).join(',');
    var query = "UPDATE m_options SET option_votes = option_votes + 1 WHERE id in ("+values+")";
    conn.query(query, (err, rows) => {
      if(err) {
        return res.json({ status:0, msg:'Unable to insert question!', error:err });
      }
      if(rows.affectedRows) {
		    res.status(200).json({ status:1, data:rows });
        conn.release();
      }
    });
    conn.on('error', (err) => {
      return res.json({ status:0, msg:'DB Connection problem!', error:err });
    });
  });
});

module.exports = router;
